from cgitb import text
from datetime import date
from gettext import install
from os import urandom
from urllib import request
import requests
import json
from string import Template #Para crear html dinámico

#Se guarda api_key (entregada cuando me registro en la pagina de la nada) como str y se define funcion para obtener data de fotos desde url de la nasa
#queda guardado como "dict"
api_key = "pXHIRZy2j2wLSnPel3dzU5Hrlo7U2dZP5MJo1jU1"

def request_get(url):
    respuesta = json.loads(requests.get(url + api_key).text)
    return respuesta


# #print(request_get("https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/latest_photos?api_key="))    
# #print(type(request_get("https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/latest_photos?api_key=")))


#Se escogeran y guardaran las primeras 25 fotos desde la data donde la lista que puedo recorrer esta definida en la variable "latest_photos", que guarda la informaci{on en diccionarios para cada id. 
data = request_get("https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/latest_photos?api_key=")["latest_photos"][:25]

#guardar url de fotos guardado en la variable "img_src" en una lista vacia

url_fotos = []
for u in data:
    url_fotos.append((u["img_src"], u["earth_date"]))
    

#crear funcion build_web_page

def build_web_page(lista1):
    #variable para agregar al inicio de cada url de la lista el str img src=
    img_template = Template("<center><img src=$elemento width=800 heiht=800 border=5 vspace=25/></center><div><KBD><center>$date</center></kbd></div>")

    #plantilla html
    html_template = Template('''<!DOCTYPE html>
                                <html>
                                <head>
                                <title>API MARS ROVER</title>
                                </head>
                                <body bgcolor=#C8C2C0>

                                <h1><center><B>25 FOTOS DE MARS ROVER</b></center></h1>
                                <h2><I><center><FONT COLOR=#CC0033>Imagenes de los Rover Curiosity, Opportunity y Spirit recopiladas consultando API de la NASA</font></center></i></h2>

                                $body

                                <h3><center><EM>By Ma.Ignacia Araya</em></center></h3>
                                </body>
                                </html>
                            ''')



    #crear str de los url agregando salto de linea para que se visualicen en la pagina hacia abajo
    texto_img = ""
    for elemento in url_fotos:
        texto_img = texto_img + img_template.substitute(elemento = elemento[0], date = elemento[1]) + "\n"
       
    #sustituit el body por todo el str con url de las imagenes

    html = html_template.substitute(body = texto_img)
    return html


#creacion de html con funcion que crea el cuerpo de la pagina web
with open("output.html", "w") as f:
    f.write(build_web_page(url_fotos)) 

#print(len(request_get("https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/latest_photos?api_key=")["latest_photos"]))

#data = request_get("https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/latest_photos?api_key=pXHIRZy2j2wLSnPel3dzU5Hrlo7U2dZP5MJo1jU1")[0:25]
#print(data)